import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './components/login';
import Signup from './components/signup';
import App from './App';

const Routes = () => (
	<Router>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/list" component={Login} />
      <Route path="/signup" component={Signup} />
    </div>
	</Router>
);
 
export default Routes;