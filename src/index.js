import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route , Switch} from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import Login from './components/login';
import Signup from './components/signup';
import VideoList from './components/videoList';
import Profile from './components/profile';
import ContactUs from './components/contactUs';
import About from './components/about';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const NoMatch = () => (
  <Redirect to={{ pathname: "/" }} />
)

ReactDOM.render(
	
		<Router>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/about" component={About} />
        <Route path="/video" component={VideoList} />
        <Route path="/signup" component={Signup} />
        <Route path="/profile" component={Profile} />
        <Route path="/contactUs" component={ContactUs} />
        <Route component={NoMatch} />
      </Switch>
  	</Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
