import React, { Component } from 'react';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import AccountCircle from '@material-ui/icons/AccountCircle';
import VideoCall from '@material-ui/icons/VideoCall';
import Videocam from '@material-ui/icons/Videocam';
import PlayCircleOutline from '@material-ui/icons/PlayCircleOutline';


const styles = theme => ({
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  link:{
    color: '#FFF', 'text-decoration': 'none', 
    outline: 'none'
  },

  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
});

class Header extends Component {

  state = {
    anchorEl: null,
    anchorEl_live:null,
    open: true,
    open_1: true,
  };

  handleChange = event => {
    this.setState({ auth: event.target.checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleLive = event => {
    this.setState({ anchorEl_live: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
    this.setState({ anchorEl_live: null });
  };

  handleCloseLive = () => {
    this.setState({ anchorEl_live: null });
  };

  render() {
    const { anchorEl } = this.state;
    const { anchorEl_live } = this.state;
    const open = Boolean(anchorEl);
    const open_1 = Boolean(anchorEl_live);
    const { classes } = this.props;

    return (
      <AppBar className={classNames(classes.appBar, this.state.open_1||this.state.open && classes.appBarShift)} style={{ position: 'fixed', top: 0 }}>
        <Toolbar className={classes.toolbar}>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            Rating Talent 
          </Typography>
          <div className={classes.grow} />
          { this.props.searchBar &&
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
              />
            </div>
          }

          {/* Go live section */}
            <Menu id="menu-appbar" 
              anchorEl={anchorEl_live}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }} open={open_1} onClose={this.handleCloseLive} >
              <MenuItem>
                <IconButton
                  aria-owns='menu-appbar' aria-haspopup="true" color="inherit" >
                  <PlayCircleOutline />
                </IconButton>
                Go Live
              </MenuItem>
              <MenuItem>
                <IconButton
                  aria-owns='menu-appbar' aria-haspopup="true" color="inherit" >
                  <Videocam />
                </IconButton>Upload video</MenuItem>
            </Menu>
          {/* end Go live section */}

          <Link className={classes.link} to="/"><Button color="inherit">Login</Button></Link>
          <Link className={classes.link} to="/about"><Button color="inherit">About</Button></Link>
          <Link className={classes.link} to="/contactUs"><Button color="inherit">ContactUs</Button></Link>
          <Link className={classes.link} to="/video"><Button color="inherit">Video</Button></Link>
          <Link className={classes.link} to="/signup"><Button color="inherit">Signup</Button></Link>
          <IconButton
            aria-owns={open_1 ? 'menu-appbar' : undefined} aria-haspopup="true"
            onClick={this.handleLive} color="inherit">
            <VideoCall />
          </IconButton>
          <IconButton
            aria-owns={open ? 'menu-appbar' : undefined} aria-haspopup="true"
            onClick={this.handleMenu} color="inherit">
            <AccountCircle />
          </IconButton>

          <Menu id="menu-appbar" anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }} open={open} onClose={this.handleClose} >
            <Link className={classes.link} to="profile"><MenuItem onClick={this.handleClose}>Profile</MenuItem></Link>
            <MenuItem onClick={this.handleClose}>Logout</MenuItem>
          </Menu>

        </Toolbar>
      </AppBar>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Header);