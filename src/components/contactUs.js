import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import PermContactCalendar from '@material-ui/icons/PermContactCalendar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import VideoClass from './videoPlayer';
import StarRating from './starRating';
import TopRatedVideo from './topRatedVideo';
import Footer from './footer';
import Header from './header';

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%'
  },
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      // marginLeft: 'auto',
      // marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1300,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    display: 'flex',
    marginTop: theme.spacing.unit * 0.2,
  },
  videoList:{ 
    width: 200,
  },
});

class ContactUs extends Component {
  render() {
    const { classes } = this.props;
    const cards = [1, 2, 3, 4, 5, 6, 7];
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header />
        <main className={classes.main}>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            <Grid container spacing={24} >
              <Grid item xl={8} xs={12} sm={8} md={8}>
                <Paper className={classes.paper}>
                  <Avatar className={classes.avatar}>
                    <PermContactCalendar />
                  </Avatar>
                  <Typography component="h1" variant="h5">
                    Contact Us
                  </Typography>

                  <form className={classes.form}>
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel>First Name</InputLabel>
                      <Input id="firstname" name="firstname" autoComplete="First Name" autoFocus />
                    </FormControl>
                    <FormControl margin="normal" fullWidth>
                      <InputLabel>Last Name</InputLabel>
                      <Input name="lastname" type="text" id="lastname" autoComplete="lastname" />
                    </FormControl>
                    
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel>Email</InputLabel>
                      <Input id="email" name="email"/>
                    </FormControl>

                    <FormControl margin="normal" fullWidth>
                      <TextField label="Comment or Meassage" multiline />
                    </FormControl>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}>
                      submit
                    </Button>
                  </form>
                </Paper>
              </Grid>
              
              <Grid item xl={4} xs={12} sm={4} md={4} >
                <TopRatedVideo/>
              </Grid>
            </Grid>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

ContactUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactUs);