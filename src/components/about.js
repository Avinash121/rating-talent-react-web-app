import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

import VideoClass from './videoPlayer';
import StarRating from './starRating';
import TopRatedVideo from './topRatedVideo';
import Header from './header';
import Footer from './footer';

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%'
  },
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      // marginLeft: 'auto',
      // marginRight: 'auto',
    },
  },
  
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1300,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  videoList:{ 
    width: 200,
  }
});

class About extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header />
        <main className={classes.main}>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            <Grid container spacing={24} >
              <Grid item xl={8} xs={12} sm={8} md={8}>
                <h3>About Page</h3>
              </Grid>
              
              <Grid item xl={4} xs={12} sm={4} md={4} >
                <TopRatedVideo/>
              </Grid>
            </Grid>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

About.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(About);