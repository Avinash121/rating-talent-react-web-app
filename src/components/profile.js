import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import Header from './header';
import VideoClass from './videoPlayer';
import StarRating from './starRating';
import businessman from '../assets/images/businessman.png';

const drawerWidth = 240;

const styles = theme => ({
	root: {
    flexGrow: 1,
    width: '100%'
  },

  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1300,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  toolbarMain: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
  },
  mainFeaturedPost: {
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing.unit * 4,
  },
  mainFeaturedPostContent: {
    padding: `${theme.spacing.unit * 5}px`,
    [theme.breakpoints.up('md')]: {
      paddingRight: 0,
    },
  },
  mainGrid: {
    marginTop: theme.spacing.unit * 3,
  },
  card: {
    display: 'flex',
    marginTop: theme.spacing.unit * 0.2,
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 150,
    borderRadius: '50%',
    border: '2px solid #9E9E9E',
    margin: theme.spacing.unit * 0.3,
  },
  videoList:{ 
  	width: 190
  },
  sidebarAboutBox: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing.unit * 3,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 6}px 0`,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth
  },
  pointer:{ cursor: 'pointer'},

  btn:{
  	float:'right',
    color: '#3f51b5',
    fontWeight: 'bold',
    fontSize: 'small',
    cursor: 'pointer'
  }
});


class Profile extends Component {
	
  render() {
    
    const sidebarData=[
      {text: 'My Profile', icon: 'account_circle'},
      {text: 'Upload Videos', icon: 'cloud_upload'},
      {text: 'My Videos', icon: 'missed_video_call'},
    ];
  	const { classes } = this.props;
		const featuredPosts = [{
		    name: 'Avinash Jain',
		    phone_number: '(123) 456-7890',
		    email:'avinash.j@geitpl.com',
		  },
		];
		const cards = [1, 2, 3, 4, 5, 6, 7];
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header />

        <main className={classes.content}>
        	<div className={classNames(classes.layout, classes.cardGrid)}>
	          {/* Sub featured posts */}
	          <Grid container spacing={24} >
	          {/*start upload section*/}
		          <Grid item xl={2} xs={12} sm={12} md={2}>
                <List>
                  {sidebarData.map((data, index) => (
                    <ListItem button key={data.text}>
                      <ListItemIcon><i class="material-icons">{data.icon}</i></ListItemIcon>
                      <ListItemText primary={data.text} />
                    </ListItem>
                  ))}
                </List>
              </Grid>
            {/*end upload section*/}
            {/*start profile section*/}
	            {featuredPosts.map(post => (
	              <Grid item key={post.title} xl={6} xs={12} sm={12} md={6} >
	                <Card className={classes.card}>
	                	<Hidden xsDown>
	                    <CardMedia
	                      className={classes.cardMedia}
	                      image={businessman} // eslint-disable-line max-len
	                      title="Image title"
	                    />
	                  </Hidden>
	                  <div className={classes.cardDetails}>
	                    <CardContent>
	                      <Typography component="h2" variant="h5">
	                        {post.name} 
	                        <span className={classes.btn}>Edit</span>
	                      </Typography>
	                      <Typography variant="subtitle1" color="textSecondary">
	                        {post.phone_number} 
	                        <span className={classes.btn}>Edit</span>
	                      </Typography>
	                      <Typography variant="subtitle1">
	                        {post.email} 
	                        <span className={classes.btn}>Edit</span>
	                      </Typography>
	                      <Typography variant="subtitle1" className={classes.pointer} color="primary">
	                        Change Profile Picture
	                      </Typography>
	                    </CardContent>
	                  </div>
	                </Card>
	              </Grid>
	            ))}
	          {/*end profile section*/}
	            <Grid item xl={4} xs={12} sm={12} md={4} >
	            	<Typography variant="h5">
                	Top Rated Videos
                </Typography>
                {cards.map(card => (
		              <Card key={card} className={classes.card}>
	                  <div className={classes.videoList}> <VideoClass/></div>
	                  <div className={classes.cardDetails}>
	                    <CardContent>
	                      <Typography variant="subtitle1">
	                        Video name
	                      </Typography>
	                      <span style={{textAlign: 'center'}}><StarRating /></span>
	                    </CardContent>
	                  </div>
		              </Card>
	            	))}
	            </Grid>
	          </Grid>
	          {/* End sub featured posts */}
          </div>
        </main>
      </div>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Profile);