import React, { Component } from 'react';
import { Player, BigPlayButton, PosterImage } from 'video-react';
import poster from '../assets/images/poster.png';

export default class VideoClass extends Component{

	constructor(props, context) {
    super(props, context);
  }
  changeThis () {
    console.log("Ended");
  }
  play() {
    this.refs.player.play();
  }
  changeCurrentTime(){
    debugger
  }
	render(){
		return (
        <Player ref="player" poster={poster} onEnded={this.changeThis.bind(this)} controls>
          <BigPlayButton position="center"  />
          <source onClick={this.changeCurrentTime} src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4" />
        </Player>
  	);
	}
}