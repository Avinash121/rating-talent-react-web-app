import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import VideoClass from './videoPlayer';
import StarRating from './starRating';

const styles = theme => ({
  card: {
    display: 'flex',
    marginTop: theme.spacing.unit * 0.2,
  },
  videoList:{ 
    width: 200,
  },
});

class TopRatedVideo extends Component {
  render() {
    const { classes } = this.props;
    const cards = [1, 2, 3, 4, 5, 6, 7];
    return (
      <div>
        <Typography variant="h6">
          <i class="material-icons">live_tv</i> Top Rated Videos
        </Typography>
        {cards.map(card => (
          <Card key={card} className={classes.card}>
            <div className={classes.videoList}> <VideoClass/></div>
            <div>
              <CardContent>
                <Typography variant="subtitle1">
                  Video name
                </Typography>
                <span style={{textAlign: 'center'}}><StarRating /></span>
              </CardContent>
            </div>
          </Card>
        ))}
      </div>
    );
  }
}

TopRatedVideo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopRatedVideo);