import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import classNames from 'classnames';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';

import businessman from '../assets/images/businessman.png';
import VideoClass from './videoPlayer';
import Header from './header';
import StarRating from './starRating';
import TopRatedVideo from './topRatedVideo';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%'
  },

  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1300,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  toolbarMain: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
  },
  mainFeaturedPost: {
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing.unit * 4,
  },
  mainFeaturedPostContent: {
    padding: `${theme.spacing.unit * 5}px`,
    [theme.breakpoints.up('md')]: {
      paddingRight: 0,
    },
  },
  mainGrid: {
    marginTop: theme.spacing.unit * 3,
  },
  card: {
    display: 'flex',
    marginTop: theme.spacing.unit * 0.2,
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 150,
    borderRadius: '50%',
    border: '2px solid #9E9E9E',
    margin: theme.spacing.unit * 0.3,
  },
  videoList:{ 
    width: 200,
  },
  sidebarAboutBox: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing.unit * 3,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 6}px 0`,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth
  },
  pointer:{ cursor: 'pointer'},

  btn:{
    float:'right',
    color: '#3f51b5',
    fontWeight: 'bold',
    fontSize: 'small',
    cursor: 'pointer'
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  live:{
    color:'red',
    backgroundColor: 'white', fontWeight: 'bold', 
    marginBottom: theme.spacing.unit * 0.6
  }
});
 
class VideoList extends Component {
  state = { searchBar: true, ww:150 }
  
  render() {
    const sidebarData=[
      {text: 'My Profile', icon: 'account_circle'},
      {text: 'Upload Videos', icon: 'cloud_upload'},
      {text: 'My Videos', icon: 'missed_video_call'},
    ];
    const { classes } = this.props;
    const featuredPosts = [{
        name: 'Avinash Jain',
        phone_number: '(123) 456-7890',
        email:'avinash.j@geitpl.com',
      },
    ];
    const cards = [1, 2, 3, 4, 5, 6, 7];
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header searchBar={this.state.searchBar}/>

        <main className={classes.content}>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            {/*start live video section*/}
            <Grid container spacing={24} >
              <Grid item xl={2} xs={12} sm={12} md={2}>
                <Typography variant="h6">
                  <i class="material-icons">videocam</i> Live Videos
                </Typography>
                {cards.map((card, j) => (
                  <div key={j} className={classes.videoList}>
                    <VideoClass />
                    <div className={classes.live}>
                      <span>LIVE</span>
                    </div>
                  </div>
                ))}
              </Grid>
            {/*end live video section*/}

            {/*start video section*/}
              {featuredPosts.map((post,i) => (
                <Grid item key={i} xl={6} xs={12} sm={12} md={6} >
                  <div style={{ padding: 20 }}>
                    <Grid container spacing={24} style={{ marginLeft: 'auto' }} >
                      {cards.map((card, k) => (
                        <div key={k} style={{margin: '1px'}} className={classes.videoList}> 
                          <VideoClass/>
                          <Card className={classes.card}>
                            <Typography align="center" color="textPrimary" >
                              Album layout
                            </Typography>
                            <span style={{textAlign: 'center'}}><StarRating /></span>
                          </Card>
                        </div>
                      ))}
                    </Grid>
                  </div>
                </Grid>
              ))}
            {/*end video section*/}

              <Grid item xl={4} xs={12} sm={12} md={4} >
                <TopRatedVideo/>
              </Grid>
            </Grid>
            {/* End sub featured posts */}
          </div>
        </main>
      </div>
    );
  }
}

VideoList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VideoList);