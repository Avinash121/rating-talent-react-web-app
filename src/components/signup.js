import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Header from './header';
import Footer from './footer';
const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%'
  },
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1300,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

class Signup extends Component {
  render() {
  	const { classes } = this.props;

    return (
    	<div >
        <CssBaseline />
      	<Header />
	      <main className={classes.main}>
	      	<div className={classNames(classes.cardGrid)}>
			      <Paper className={classes.paper}>
			        <Avatar className={classes.avatar}>
			          <LockIcon />
			        </Avatar>
			        <Typography component="h1" variant="h5">
			          Sign up
			        </Typography>

			        <form className={classes.form}>
			          <FormControl margin="normal" required fullWidth>
			            <InputLabel>First Name</InputLabel>
			            <Input id="firstname" name="firstname" autoComplete="First Name" autoFocus />
			          </FormControl>
			          <FormControl margin="normal" fullWidth>
			            <InputLabel>Last Name</InputLabel>
			            <Input name="lastname" type="text" id="lastname" autoComplete="lastname" />
			          </FormControl>
			          
			          <FormControl margin="normal" required fullWidth>
			            <InputLabel>Email</InputLabel>
			            <Input id="email" name="email"/>
			          </FormControl>

			          <FormControl margin="normal" required fullWidth>
			            <InputLabel>Password</InputLabel>
			            <Input name="password" type="password" id="password" />
			          </FormControl>


			          <FormControl margin="normal" required fullWidth>
			            <InputLabel>Confirm Password</InputLabel>
			            <Input name="confirm_pwd" type="password" id="confirm_pwd" />
			          </FormControl>

			          <Button
			            type="submit"
			            fullWidth
			            variant="contained"
			            color="primary"
			            className={classes.submit}
			          >
			            Sign up
			          </Button>
			        </form>
			      </Paper>
		      </div>
	    	</main>
    		<Footer/>
    	</div>
    );
  }
}

Signup.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Signup);