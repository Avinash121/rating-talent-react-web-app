import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';

class StarRating extends Component {
	state={rating: 1}

	changeRating = event => {
    this.setState({ rating: event });
  };

  render() {	
    return (
      	 <StarRatings
          rating={this.state.rating}
          starRatedColor="#ff0000eb"
          starHoverColor="#3f51b5"
          changeRating={this.changeRating}
          numberOfStars={7}
          name='rating'
          starDimension="20px"
          starSpacing="1px"
        />
    );
  }
}

export default StarRating;